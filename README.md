GULP TASKS:

- `gulp` to build an optimized version of application in folder dist
- `gulp serve` to start BrowserSync server on source files with live reload
- `gulp serve:dist` to start BrowserSync server on optimized application without live reload
