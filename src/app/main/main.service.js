(function() {
  'use strict';

  angular.module('bingo').factory('BingoFactory', BingoFactory);

  /** @ngInject */
  function BingoFactory($log, $http) {
    var apiHost = 'http://52.30.37.247';

    var service = {
      getCard: getCard,
      getNumbers: getNumbers,
      startGame: startGame,
      checkIfNumberIsWinning: checkIfNumberIsWinning
    };

    return service;

    function getCard() {
      return $http.get(apiHost + '/bingo.php?action=get_card')
        .then(function(response) {
          var numbers = [];
          for(var i=0; i<response.data.length; i++) {
            numbers.push({
              'value': response.data[i],
              'win': false
            });
          }
          return numbers;
        })
        .catch(function(error) {
          $log.error('XHR Failed for getCard.\n' + angular.toJson(error.data, true));
        });
    }

    function getNumbers() {
      return $http.get(apiHost + '/bingo.php?action=get_numbers')
        .then(function(response) {
          return response.data;
        })
        .catch(function(error) {
          $log.error('XHR Failed for getNumbers.\n' + angular.toJson(error.data, true));
        });
    }

    function startGame() {
      return $http.get(apiHost + '/bingo.php?action=start_game')
        .then(function(response) {
          return response.data;
        })
        .catch(function(error) {
          $log.error('XHR Failed for startGame.\n' + angular.toJson(error.data, true));
        });
    }

    function checkIfNumberIsWinning(number, array) {
      for(var i=0; i<array.length; i++) {
        if(number === array[i]) {
          return true;
        }
      }
      return false;
    }
  }
})();
