(function() {
  'use strict';

  angular.module('bingo').controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, BingoFactory) {
    var vm = this;
    vm.cards = [];
    vm.numbers = [];
    vm.getCard = getCard;
    vm.startGame = startGame;

    function startGame() {
        BingoFactory.startGame().then(function(data) {
          if(data[0] ==='ok') {
            updateNumbers();
          }
        });
    }

    function updateNumbers() {
      getNumbers();

      $timeout(function () {
        if(vm.numbers[vm.numbers.length-1]!=='end') {
          updateNumbers();
        }
      }, 2000);
    }

    function getCard() {
      BingoFactory.getCard().then(function(data) {
        vm.cards.push(data);
      });
    }

    function getNumbers() {
      BingoFactory.getNumbers().then(function(data) {
        vm.numbers = data;

        checkResults();
      });
    }

    function checkResults() {
      var i, j, numbersLength,
      cardsLength = vm.cards.length;

      for(i=0; i<cardsLength; i++) {
        numbersLength = vm.cards[i].length;

        for(j=0; j<numbersLength; j++) {
          vm.cards[i][j].win = BingoFactory.checkIfNumberIsWinning(vm.cards[i][j].value, vm.numbers);
        }
      }
    }
  }
})();
